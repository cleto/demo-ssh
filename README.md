# Demo SSH

## Instrucciones

1. Reemplaza `docker/assets/authorized_keys.op` y
   `docker/jump/authorized_keys.jump` por tu clave pública de el
   usuario `op`.

1. `make remove containers`

Para saber si todo va bien, el siguiente comando debe funcionar:

```
ssh -J jump@172.17.0.2 -A op@10.0.3.3  # y desde work "ssh op@10.0.1.2"
```

Recuerda que es posible que tengas que añadir manualmente la clave
privada de `op` a tu SSH Agent para que funcione con:

```
ssh-add /path/clave/privada
```

Los siguientes comandos _no_ deben funcionar:

```
ssh jump@172.17.0.2
ssh op@10.0.3.3
```

## Falta

El usuario `dev` debe estar configurado de forma que se pueda:

```
ssh -J jump@172.17.0.2 -A dev@10.0.3.3
```
